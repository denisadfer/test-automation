package pageObject.generalFunction;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Collections of action methods to interact with the UI
 */
public class GeneralAction extends BaseGeneralFunction {

    SupportFunction sprt;

    public GeneralAction(WebDriver driver) {
        super(driver);
        sprt = new SupportFunction(driver);
    }

    /**
     * Click an element
     * 
     * @param locator element locator
     */
    public void clickElement(By locator) {
        sprt.getElementFluently(locator).click();
    }

    /**
     * Click an element with specified timeout
     * 
     * @param locator element locator
     * @param timeout in seconds
     */
    public void clickElement(By locator, int timeout) {
        sprt.getElementFluently(locator, timeout).click();
    }

    /**
     * Simulate user typing on specific element (Usually for input boxes)
     * 
     * @param locator target element locator
     * @param text    text to be typed
     */
    public void typeText(By locator, String text) {
        driver.findElement(locator).sendKeys(text);
    }
}