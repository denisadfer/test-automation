package stepDefinition;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import factory.DriverFactory;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObject.generalFunction.GeneralAction;
import pageObject.generalFunction.GeneralFetch;
import pageObject.generalFunction.SupportFunction;

public class GeneralStep {

    WebDriver driver;
    Actions action;
    GeneralAction genAction;
    GeneralFetch genFetch;
    SupportFunction sprt;

    public static SoftAssertions softAssert = new SoftAssertions();

    /**
     * Initialize GeneralStep instance using default driver from
     * {@code DriverFactory}.
     */
    public GeneralStep() {
        this.driver = DriverFactory.getDriver();
        this.action = new Actions(this.driver);
        this.genAction = new GeneralAction(driver);
        this.genFetch = new GeneralFetch(driver);
        this.sprt = new SupportFunction(driver);
    }

    /**
     * Initialize GeneralStep instance using custom driver.
     * 
     * @param driver
     */
    public GeneralStep(WebDriver driver) {
        this.driver = driver;
        this.action = new Actions(driver);
        this.genAction = new GeneralAction(driver);
        this.genFetch = new GeneralFetch(driver);
        this.sprt = new SupportFunction(driver);
    }

    @When("the user refresh the browser page")
    public void user_refresh_the_browser_page() {
        driver.navigate().refresh();
    }

    @Then("the page's title should be {string}")
    public void validatePageTitle(String expectedTitle) {
        assertEquals(expectedTitle, sprt.getPageTitle());
    }

    @Then("a text as {string} is shown on the screen")
    public void validateTextShown(String exactText) {
        By textLocator = By.xpath("//*[text()='" + exactText + "']");
        assertTrue("A text \"" + exactText + "\" cannot be found", genFetch.isElementExist(textLocator));
    }

    @When("the user click on the {string} button")
    public void user_clicks_on_button(String button) {
        By btnLocator = By.xpath("//button[text()='" + button + "']");
        genAction.clickElement(btnLocator);
    }

    @Then("the {string} button should be {string} on the screen")
    public void the_button_should_be_on_the_screen(String button, String state) {
        By btnLocator = By.xpath("//button[text()='" + button + "']");
        Boolean actualButtonState = genFetch.isButtonEnabled(btnLocator);
        // Simple if-else
        if(state.equalsIgnoreCase("enabled")){
            assertTrue(button + " button is not enabled!", actualButtonState);
        }
        else{
            assertFalse(button + " button is not disabled!", actualButtonState);
        }
    }

    @When("the user click the {string} text on the screen")
    public void userClicks_anyText(String exactText) {
        By textLocator = By.xpath("//*[text()='" + exactText + "']");
        // Validate that the text is present before continuing
        validateTextShown(exactText);
        genAction.clickElement(textLocator);
    }

    @And("wait for loading process")
    public void waitLoadingProcess() throws InterruptedException {
        /*
         * Just a simple thread sleep.
         * Real usage must use dynamic wait to maintain responsiveness.
         */
        Thread.sleep(200);
    }

    @And("wait for {int} ms")
    public void waitSpecifiedTime(int waitTime) throws InterruptedException {
        Thread.sleep(waitTime);
    }

    @Then("the user is navigated to the {string} page")
    public void the_user_is_navigated_to_the_page(String page) {
        if (page.equalsIgnoreCase("home")) {
            String identifiableText = "Protocol Explorer";
            By homeLocator = By.xpath("//*[text()='" + identifiableText + "']");
            assertTrue("The user is not on the " + page + " page.", genFetch.isElementExist(homeLocator));
        }
    }

    /**
     * Used to check whether a text is shown or not
     * without halting the automation script.
     * <br>
     * </br>
     * You Need to implement "<b>Then re-validate soft-assert</b>"
     * gherkin steps in the end to use it properly,
     * otherwise it might miss any (text) Bug/Typo.
     * 
     * @param targetText
     */
    @Then("optionally validate a text as {string} is shown on the screen")
    public void checktextPresence_Optionally(String targetText) {
        By textLocator = By.xpath("//*[text()='" + targetText + "']");
        sprt.setImplicitWaitMillis(500);
        Boolean isFailed = false;
        Boolean isTextShown = genFetch.isElementExist(textLocator);
        isFailed = isTextShown ? false : true;

        if (isFailed) {
            softAssert.fail("==> The text can't be found when verifying '" + targetText + "'");
            // Set checked to false to ensure it's checked later
            sprt.setSoftAssertCheckedStatus(false);
        }
        // If it's not failed, its and indication that the text is present.
        // Therefore, Soft-Assertion is not required anymore.
        sprt.resetImplicitWait();
    }

    @Then("re-validate soft-assert")
    public void soft_assert_checks() {
        sprt.setSoftAssertCheckedStatus(true);
        softAssert.assertAll();
    }
}
