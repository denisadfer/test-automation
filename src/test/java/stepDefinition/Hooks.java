package stepDefinition;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import factory.DriverFactory;
import infrastructure.config.Configuration;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import pageObject.generalFunction.SupportFunction;

/**
 * The Hooks class contain all method to implement all {@code Hooks} that
 * implemented for the {@code Cucumber} framework.
 * <p>
 * Available Hooks :
 * {@code Before, After, BeforeStep, AfterStep, BeforeAll, AfterAll}
 * 
 * @see https://cucumber.io/docs/cucumber/api/?lang=java#hooks
 * @version 1.0
 */
public class Hooks {
    private DriverFactory driverFactory;
    private WebDriver driver;
    private Configuration config;
    private String url;
    SupportFunction sprt;

    public Hooks() {
        config = new Configuration();
        driverFactory = new DriverFactory();
        // import variables from config file
        config.initProp();
        url = config.URL;
    }

    @Before(order = 0)
    public void launchBrowser() {
        driver = driverFactory.init(config.browser, config.isHeadless);
        driver.get(url);
    }

    @Before(order = 1)
    public void setSoftAssert() {
        sprt = new SupportFunction(driver);
    }

    @After(order = 2)
    public void validateSoftAssert() {
        Boolean isChecked = sprt.getSoftAssertCheckedStatus();
        if (!isChecked) {
            String msg = " ->> WARNING: Detected Soft-assertion that is not re-validated yet!";
            System.out.println(msg);
        }
    }

    @After(order = 1)
    public void tearDown(Scenario scenario) throws IOException {
        Boolean enableScreenshot = config.enableScreenshot;

        if (scenario.isFailed()) {
            if (enableScreenshot) {
                String screenshotName = scenario.getName().replaceAll(" ", "_");
                byte[] sourcePath = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.attach(sourcePath, "image/png", screenshotName);
            } else {
                String screenshotName = scenario.getName();
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                FileUtils.copyFile(scrFile, new File("/output/screenshot/" + screenshotName + ".png"));
            }
        }
    }

    @After(order = 0)
    public void closeDriver() {
        driver.quit();
    }
}
