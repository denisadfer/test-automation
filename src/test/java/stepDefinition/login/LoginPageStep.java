package stepDefinition.login;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import factory.DriverFactory;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.*;
import pageObject.login.LoginPage;

/**
 * Step Definition class that provides
 * gherkin syntaxes for UI interaction on the
 * <b>Login Page</b>
 */
public class LoginPageStep {

	private LoginPage loginPage;

	public LoginPageStep() {
		loginPage = new LoginPage(DriverFactory.getDriver());
	}

	@Given("the user is on the Login page")
	public void user_is_on_Login_page() {
		assertTrue("The user is not on the login page.", loginPage.isLoginButtonDisplayed());
	}

	@When("the user enters the username as {string}")
	public void user_enters_username(String username) {
		loginPage.typeUsername(username);
	}

	@And("the user enters the password as {string}")
	public void user_enters_password(String password) {
		loginPage.typePassword(password);
	}

	@And("the user click the Login button on the Login Page")
	public void clickLogin() {
		loginPage.clickLoginBtn();
	}

	@Given("the user already logged in to application with this credentials")
	public void user_has_already_logged_in_to_application_with_this_credentials(DataTable credTable) {
		List<Map<String, String>> credList = credTable.asMaps();
		String username = credList.get(0).get("Username");
		String password = credList.get(0).get("Password");
		loginPage.loginToSystem(username, password);
	}
}
