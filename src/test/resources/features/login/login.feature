@Login @Test
Feature: Login feature

    Check login functionality

    Background: The user is already navigated to the login page
        Given the user is on the Login page
        Then the page's title should be "Formulatrix Engineering Review System"

    Scenario: TC-1:Login will success with valid credentials
        Precondition : Already created the "hoeko.setyawijaya@formulatrix.com" account

        When the user enters the username as "hoeko.setyawijaya@formulatrix.com"
        And the user enters the password as "123456"
        And the user click the Login button on the Login Page
        Then the user is navigated to the "Dashboard" page
        Then the page's title should be "Formulatrix Engineering Review System"

    Scenario: TC-2:Login will not success with invalid credentials
        User are unable to login if credentials are invalid

        When the user enters the username as "guest"
        And the user enters the password as "guest"
        And the user click the Login button on the Login Page
        And wait for 1000 ms
        Then a text as "Username or password not found" is shown on the screen
        Then the page's title should be "Formulatrix Engineering Review System"

    Scenario: TC-3:Login will allowed with complete credentials
        User are able to click login if credentials are complete

        When the user enters the username as "guest"
        And the user enters the password as "guest"
        Then the "Login" button should be "enabled" on the screen

    Scenario: TC-4:Login will not allowed with incomplete credentials (Username)
        User are unable to login if credentials are incomplete

        When the user enters the username as "guest"
        Then the "Login" button should be "disabled" on the screen


    Scenario: TC-5:Login will not allowed with incomplete credentials (Password)
        User are unable to login if credentials are incomplete

        When the user enters the password as "guest"
        Then the "Login" button should be "disabled" on the screen

    @Example
    Scenario: The user already logged in
        Precondition: The user is already on the home page

        Given the user already logged in to application with this credentials
            | Username                          | Password |
            | hoeko.setyawijaya@formulatrix.com | 123456   |
        And wait for 1000 ms
        Then the user is navigated to the "Dashboard" page